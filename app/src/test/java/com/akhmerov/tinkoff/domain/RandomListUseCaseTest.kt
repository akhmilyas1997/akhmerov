package com.akhmerov.tinkoff.domain

import com.akhmerov.tinkoff.core.ApiResponse
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class RandomListUseCaseTest {

    private val staticGif = RandomGif("test", "https://gif.url", 123)

    private inner class RandomRepositorySuccessTest : RandomRepository {
        override val gifList: MutableStateFlow<List<ApiResponse<RandomGif>?>> = MutableStateFlow(emptyList())

        override suspend fun updateGifByIndex(index: Int) {
            val newList = gifList.value.toMutableList()
            newList.add(index, ApiResponse.Success(staticGif))
            gifList.value = newList
        }
    }

    private lateinit var useCaseWithSuccessRepo: RandomListUseCaseImpl

    @Before
    fun initUseCase() {
        useCaseWithSuccessRepo = RandomListUseCaseImpl(RandomRepositorySuccessTest())
    }

    @Test
    fun `Test moving with getNext and getPrev`() = runTest {
        useCaseWithSuccessRepo.updateCurrent()
        Assert.assertTrue(useCaseWithSuccessRepo.currentIndex.value == 0)

        useCaseWithSuccessRepo.getNext()
        Assert.assertTrue(useCaseWithSuccessRepo.currentIndex.value == 1)

        useCaseWithSuccessRepo.getNext()
        Assert.assertTrue(useCaseWithSuccessRepo.currentIndex.value == 2)

        useCaseWithSuccessRepo.getPrev()
        Assert.assertTrue(useCaseWithSuccessRepo.currentIndex.value == 1)

        useCaseWithSuccessRepo.getPrev()
        Assert.assertTrue(useCaseWithSuccessRepo.currentIndex.value == 0)
    }

    @Test
    fun `Test get same success object as in repo`() = runTest {
        Assert.assertTrue(useCaseWithSuccessRepo.gifList.value.isEmpty())
        useCaseWithSuccessRepo.updateCurrent()
        val response = useCaseWithSuccessRepo.gifList.value.first()
        Assert.assertTrue((response as? ApiResponse.Success)?.data == staticGif)
    }
}