package com.akhmerov.tinkoff.data

import com.akhmerov.tinkoff.core.Mapper
import com.akhmerov.tinkoff.domain.RandomGif
import javax.inject.Inject

class RandomNetworkMapper @Inject constructor() : Mapper<RandomGifModel, RandomGif> {
    override fun map(from: RandomGifModel): RandomGif {
        return RandomGif(description = from.description, gifURL = from.gifURL, id = from.id)
    }
}