package com.akhmerov.tinkoff.data

import com.akhmerov.tinkoff.core.ApiResponse
import com.akhmerov.tinkoff.core.DispatcherProvider
import com.akhmerov.tinkoff.core.Mapper
import com.akhmerov.tinkoff.core.safeApiCallAndMap
import com.akhmerov.tinkoff.domain.RandomGif
import com.akhmerov.tinkoff.domain.RandomRepository
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RandomRepositoryImpl @Inject constructor(
    private val mapper: Mapper<RandomGifModel, RandomGif>,
    private val randomService: RandomService,
    private val dispatchers: DispatcherProvider,
) : RandomRepository {

    override val gifList = MutableStateFlow<List<ApiResponse<RandomGif>?>>(emptyList())

    override suspend fun updateGifByIndex(index: Int) = withContext(dispatchers.defaultDispatcher) {
        val list = gifList.value.toMutableList()
        val newGif = getNewGif()
        if (index > list.lastIndex) {
            list.add(newGif)
        } else {
            list[index] = newGif
        }
        gifList.value = list
    }

    private suspend fun getNewGif() =
        safeApiCallAndMap(mapper, dispatchers.ioDispatcher) { randomService.getRandomGif() }

}