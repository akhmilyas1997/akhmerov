package com.akhmerov.tinkoff.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RandomGifModel(
    @Json(name = "author")
    val author: String,
    @Json(name = "canVote")
    val canVote: Boolean,
    @Json(name = "commentsCount")
    val commentsCount: Int,
    @Json(name = "date")
    val date: String,
    @Json(name = "description")
    val description: String,
    @Json(name = "fileSize")
    val fileSize: Int,
    @Json(name = "gifSize")
    val gifSize: Int,
    @Json(name = "gifURL")
    val gifURL: String,
    @Json(name = "height")
    val height: String,
    @Json(name = "id")
    val id: Int,
    @Json(name = "previewURL")
    val previewURL: String,
    @Json(name = "type")
    val type: String,
    @Json(name = "videoPath")
    val videoPath: String,
    @Json(name = "videoSize")
    val videoSize: Int,
    @Json(name = "videoURL")
    val videoURL: String,
    @Json(name = "votes")
    val votes: Int,
    @Json(name = "width")
    val width: String
)