package com.akhmerov.tinkoff.data

import retrofit2.Response
import retrofit2.http.GET

interface RandomService {

    @GET("random?json=true")
    suspend fun getRandomGif(): Response<RandomGifModel>
}