package com.akhmerov.tinkoff

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import by.kirich1409.viewbindingdelegate.viewBinding
import com.akhmerov.tinkoff.core.CommonError
import com.akhmerov.tinkoff.core.ErrorProcessor
import com.akhmerov.tinkoff.databinding.FragmentMainBinding
import com.akhmerov.tinkoff.presentation.GifLoadingStatus
import com.akhmerov.tinkoff.presentation.RandomUiModel
import com.akhmerov.tinkoff.presentation.RandomViewModel
import com.akhmerov.tinkoff.utils.launchWhenStarted
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainFragment : Fragment(R.layout.fragment_main) {
    private val binding by viewBinding(FragmentMainBinding::bind)
    private val viewModel: RandomViewModel by viewModels()

    @Inject
    lateinit var errorProcessor: ErrorProcessor

    private val glideListener = object : RequestListener<Drawable> {
        override fun onLoadFailed(
            e: GlideException?,
            model: Any?,
            target: Target<Drawable>?,
            isFirstResource: Boolean
        ): Boolean {
            viewModel.onGifLoadingError()
            return false
        }

        override fun onResourceReady(
            resource: Drawable?,
            model: Any?,
            target: Target<Drawable>?,
            dataSource: DataSource?,
            isFirstResource: Boolean
        ): Boolean {
            viewModel.onGifLoaded()
            return false
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.nextButton.setOnClickListener {
            viewModel.onNextClicked()
        }

        binding.prevButton.setOnClickListener {
            viewModel.onPrevClicked()
        }

        binding.errorButton.setOnClickListener {
            viewModel.onRetryClicked()
        }

        launchWhenStarted {
            viewModel.isPrevVisible.collect { isVisible ->
                binding.prevButton.isVisible = isVisible
            }
        }

        launchWhenStarted {
            viewModel.uiModel.collect { uiModel ->
                renderUi(uiModel)
            }
        }
    }

    private fun renderUi(uiModel: RandomUiModel) {
        binding.nextButton.isVisible = uiModel.isNextVisible
        when (uiModel) {
            RandomUiModel.Loading -> {
                binding.progressBar.isVisible = true
                binding.description.isVisible = false
                setError(null)
            }
            is RandomUiModel.Success -> {
                processSuccess(uiModel)
            }
            is RandomUiModel.Error -> {
                binding.progressBar.isVisible = false
                setError(uiModel.error)
            }
        }
    }

    private fun processSuccess(model: RandomUiModel.Success) {
        glideLoad(model.gif.gifURL)
        binding.description.isVisible = true
        binding.description.text = model.gif.description
        val isProgressVisible: Boolean
        val error: CommonError?
        when (model.gifLoadingStatus) {
            GifLoadingStatus.Loading -> {
                isProgressVisible = true
                error = null
            }
            GifLoadingStatus.Success -> {
                isProgressVisible = false
                error = null
            }
            is GifLoadingStatus.Error -> {
                isProgressVisible = false
                error = model.gifLoadingStatus.error
            }
        }
        binding.progressBar.isVisible = isProgressVisible
        setError(error)
    }

    private fun setError(error: CommonError?) {
        with(binding) {
            errorButton.isVisible = error != null
            errorText.isVisible = error != null
            errorImage.isVisible = error != null
            imageCard.isVisible = error == null
            if (error != null) {
                errorText.text = errorProcessor.processError(error)
            }
        }
    }

    private fun glideLoad(url: String) {
        viewModel.gifStartLoading()
        binding.imageView.post {
            Glide
                .with(binding.imageView.context)
                .load(url)
                .centerCrop()
                .listener(glideListener)
                .into(binding.imageView)
        }
    }
}