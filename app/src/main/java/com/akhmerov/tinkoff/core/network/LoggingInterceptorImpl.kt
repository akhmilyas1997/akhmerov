package com.akhmerov.tinkoff.core.network

import okhttp3.Interceptor
import okhttp3.logging.HttpLoggingInterceptor
import java.io.IOException
import javax.inject.Inject

class LoggingInterceptorImpl @Inject constructor() : Interceptor {

    private val mLoggingInterceptor: Interceptor

    init {
        mLoggingInterceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
        return mLoggingInterceptor.intercept(chain)
    }
}