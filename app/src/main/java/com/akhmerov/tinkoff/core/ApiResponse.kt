package com.akhmerov.tinkoff.core

import com.akhmerov.tinkoff.R
import retrofit2.Response

sealed class ApiResponse<out T> {
    data class Success<T>(val data: T) : ApiResponse<T>()
    data class Error(val error: CommonError) : ApiResponse<Nothing>()

    companion object {
        inline fun <reified T> create(response: Response<T>): ApiResponse<T> {
            return if (response.isSuccessful) {
                val body = response.body()
                if (body != null) {
                    Success(body)
                } else {
                    Error(CommonError.ServerError.ServerResStringError(R.string.server_error, response.code()))
                }
            } else {
                val error = response.errorBody()?.string()
                if (error != null) {
                    Error(CommonError.ServerError.ServerStringError(error, response.code()))
                } else {
                    Error(CommonError.ServerError.ServerResStringError(R.string.server_error, response.code()))
                }
            }
        }
    }
}