package com.akhmerov.tinkoff.core

import com.akhmerov.tinkoff.R
import com.squareup.moshi.JsonDataException
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import retrofit2.Response
import java.net.UnknownHostException
import javax.net.ssl.SSLHandshakeException

suspend inline fun <reified From, To> safeApiCallAndMap(
    mapper: Mapper<From, To>,
    dispatcher: CoroutineDispatcher,
    crossinline block: suspend () -> Response<From>
): ApiResponse<To> =
    withContext(dispatcher) {
        try {
            when (val response = ApiResponse.create(block())) {
                is ApiResponse.Success -> ApiResponse.Success(mapper.map(response.data))
                is ApiResponse.Error -> ApiResponse.Error(response.error)
            }
        } catch (e: Exception) {
            when (e) {
                is SSLHandshakeException, is UnknownHostException -> {
                    ApiResponse.Error(CommonError.ResError(R.string.network_error))
                }
                is JsonDataException -> {
                    ApiResponse.Error(CommonError.ResError(R.string.data_parsing_error))
                }
                else -> {
                    ApiResponse.Error(CommonError.ThrowableError(e))
                }
            }
        }
    }