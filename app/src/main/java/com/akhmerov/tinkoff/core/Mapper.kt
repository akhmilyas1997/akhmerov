package com.akhmerov.tinkoff.core

interface Mapper<From, To> {
    fun map(from: From): To
}