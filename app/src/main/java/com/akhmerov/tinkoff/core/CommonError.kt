package com.akhmerov.tinkoff.core

import android.content.Context
import androidx.annotation.StringRes
import com.akhmerov.tinkoff.R
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

sealed class CommonError {
    data class ThrowableError(val throwable: Throwable) : CommonError()
    data class ResError(@StringRes val stringId: Int) : CommonError()
    sealed class ServerError(val responseCode: Int) : CommonError() {
        data class ServerStringError(val error: String, val responseCodeId: Int) : ServerError(responseCodeId)
        data class ServerResStringError(@StringRes val errorStringId: Int, val responseCodeId: Int) :
            ServerError(responseCodeId)
    }
}

class ErrorProcessorImpl @Inject constructor(@ApplicationContext private val context: Context) : ErrorProcessor {
    override fun processError(error: CommonError): String {
        return when (error) {
            is CommonError.ThrowableError -> error.throwable.localizedMessage
                ?: context.getString(R.string.network_error)
            is CommonError.ResError -> context.getString(error.stringId)
            is CommonError.ServerError.ServerStringError -> error.error
            is CommonError.ServerError.ServerResStringError -> context.getString(error.errorStringId)
        }
    }
}

interface ErrorProcessor {
    fun processError(error: CommonError): String
}