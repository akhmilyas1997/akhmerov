package com.akhmerov.tinkoff.domain

data class RandomGif(
    val description: String,
    val gifURL: String,
    val id: Int,
)