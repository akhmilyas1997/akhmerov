package com.akhmerov.tinkoff.domain

import com.akhmerov.tinkoff.core.ApiResponse
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.updateAndGet
import javax.inject.Inject

interface RandomListUseCase {
    val gifList: StateFlow<List<ApiResponse<RandomGif>?>>
    val currentIndex: StateFlow<Int>
    suspend fun updateCurrent()
    suspend fun getNext()
    suspend fun getPrev()
}

class RandomListUseCaseImpl @Inject constructor(private val repository: RandomRepository) : RandomListUseCase {
    override val gifList: StateFlow<List<ApiResponse<RandomGif>?>> = repository.gifList
    override val currentIndex = MutableStateFlow(0)

    override suspend fun updateCurrent() {
        repository.updateGifByIndex(currentIndex.value)
    }

    override suspend fun getNext() {
        val list = gifList.value
        val nextIndex = currentIndex.updateAndGet { it + 1 }
        if (nextIndex > list.lastIndex) {
            repository.updateGifByIndex(nextIndex)
        } else if (list.getOrNull(nextIndex) !is ApiResponse.Success) {
            repository.updateGifByIndex(nextIndex)
        }
    }

    override suspend fun getPrev() {
        val prevIndex = currentIndex.updateAndGet { it - 1 }
        if (gifList.value.getOrNull(prevIndex) !is ApiResponse.Success) {
            repository.updateGifByIndex(prevIndex)
        }
    }
}