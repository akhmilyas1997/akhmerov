package com.akhmerov.tinkoff.domain

import com.akhmerov.tinkoff.core.ApiResponse
import kotlinx.coroutines.flow.StateFlow

interface RandomRepository {
    val gifList: StateFlow<List<ApiResponse<RandomGif>?>>
    suspend fun updateGifByIndex(index: Int)
}