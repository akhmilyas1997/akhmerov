package com.akhmerov.tinkoff.utils

import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

inline fun Fragment.launchWhen(
    state: Lifecycle.State,
    crossinline action: suspend CoroutineScope.() -> Unit
): Job = viewLifecycleOwner.lifecycleScope.launch {
    viewLifecycleOwner.lifecycle.repeatOnLifecycle(state) {
        action()
    }
}

inline fun Fragment.launchWhenStarted(crossinline action: suspend CoroutineScope.() -> Unit) =
    launchWhen(Lifecycle.State.STARTED, action)

inline fun Fragment.launchWhenResumed(crossinline action: suspend CoroutineScope.() -> Unit) =
    launchWhen(Lifecycle.State.RESUMED, action)