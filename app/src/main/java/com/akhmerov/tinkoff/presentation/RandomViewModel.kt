package com.akhmerov.tinkoff.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.akhmerov.tinkoff.R
import com.akhmerov.tinkoff.core.ApiResponse
import com.akhmerov.tinkoff.core.CommonError
import com.akhmerov.tinkoff.domain.RandomGif
import com.akhmerov.tinkoff.domain.RandomListUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RandomViewModel @Inject constructor(
    private val randomListUseCase: RandomListUseCase
) : ViewModel() {

    private val gifLoadingStatus =
        MutableStateFlow<GifLoadingStatus>(GifLoadingStatus.Loading)

    val isPrevVisible = randomListUseCase.currentIndex.map { it > 0 }

    val uiModel = combine(
        randomListUseCase.gifList,
        randomListUseCase.currentIndex,
        gifLoadingStatus,
        ::transfromUiState
    )

    init {
        viewModelScope.launch { randomListUseCase.updateCurrent() }
    }

    fun onNextClicked() {
        viewModelScope.launch { randomListUseCase.getNext() }
    }

    fun onPrevClicked() {
        viewModelScope.launch { randomListUseCase.getPrev() }
    }

    fun onRetryClicked() {
        viewModelScope.launch { randomListUseCase.updateCurrent() }
    }

    fun gifStartLoading() {
        gifLoadingStatus.value = GifLoadingStatus.Loading
    }

    fun onGifLoaded() {
        gifLoadingStatus.value = GifLoadingStatus.Success
    }

    fun onGifLoadingError() {
        gifLoadingStatus.value = GifLoadingStatus.Error(CommonError.ResError(R.string.network_error))
    }

    private fun transfromUiState(
        list: List<ApiResponse<RandomGif>?>,
        index: Int,
        gifLoadingStatus: GifLoadingStatus
    ): RandomUiModel {
        return when (val element = list.getOrNull(index)) {
            is ApiResponse.Success -> {
                RandomUiModel.Success(gif = element.data, gifLoadingStatus = gifLoadingStatus)
            }
            is ApiResponse.Error -> {
                RandomUiModel.Error(error = element.error)
            }
            null -> {
                RandomUiModel.Loading
            }
        }
    }
}