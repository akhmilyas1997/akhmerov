package com.akhmerov.tinkoff.presentation

import com.akhmerov.tinkoff.core.CommonError
import com.akhmerov.tinkoff.domain.RandomGif

sealed class RandomUiModel(
    open val isNextVisible: Boolean,
) {
    object Loading : RandomUiModel(false)
    data class Success(val gif: RandomGif, val gifLoadingStatus: GifLoadingStatus) : RandomUiModel(true)
    data class Error(val error: CommonError) : RandomUiModel(false)
}

sealed class GifLoadingStatus {
    object Loading : GifLoadingStatus()
    object Success : GifLoadingStatus()
    data class Error(val error: CommonError) : GifLoadingStatus()
}