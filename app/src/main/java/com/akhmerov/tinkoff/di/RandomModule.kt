package com.akhmerov.tinkoff.di

import com.akhmerov.tinkoff.core.Mapper
import com.akhmerov.tinkoff.data.RandomGifModel
import com.akhmerov.tinkoff.data.RandomNetworkMapper
import com.akhmerov.tinkoff.data.RandomRepositoryImpl
import com.akhmerov.tinkoff.data.RandomService
import com.akhmerov.tinkoff.domain.*
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped
import retrofit2.Retrofit

@InstallIn(ActivityRetainedComponent::class)
@Module
class RandomModule {
    @Provides
    @ActivityRetainedScoped
    fun provideRandomService(retrofit: Retrofit): RandomService {
        return retrofit.create(RandomService::class.java)
    }
}

@InstallIn(ActivityRetainedComponent::class)
@Module
interface RandomBind {
    @Binds
    @ActivityRetainedScoped
    fun bindRepository(impl: RandomRepositoryImpl): RandomRepository

    @Binds
    @ActivityRetainedScoped
    fun bindNetworkMapper(impl: RandomNetworkMapper): Mapper<RandomGifModel, RandomGif>

    @Binds
    @ActivityRetainedScoped
    fun bindUseCase(impl: RandomListUseCaseImpl): RandomListUseCase
}