package com.akhmerov.tinkoff.di

import com.akhmerov.tinkoff.core.network.LoggingInterceptorImpl
import com.squareup.moshi.Moshi
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class NetworkModule {
    @Provides
    @Singleton
    fun provideMoshi(): Moshi {
        return Moshi
            .Builder()
            .build()
    }

    @Provides
    @Singleton
    fun provideOkHttp(
        @LoggingInterceptor loggingInterceptor: Interceptor
    ): OkHttpClient {
        return OkHttpClient().newBuilder()
            .connectTimeout(timeoutInSec, TimeUnit.SECONDS)
            .readTimeout(timeoutInSec, TimeUnit.SECONDS)
            .writeTimeout(timeoutInSec, TimeUnit.SECONDS)
            .addInterceptor(loggingInterceptor)
            .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(client: OkHttpClient, moshi: Moshi, @DefaultServerUrl defaultApiUrl: String): Retrofit {
        return Retrofit.Builder()
            .client(client)
            .baseUrl(defaultApiUrl)
            .addConverterFactory(MoshiConverterFactory.create(moshi).asLenient())
            .build()
    }

    @Provides
    @Singleton
    @DefaultServerUrl
    fun provideServerUrl(): String = "https://developerslife.ru/"

    companion object {
        private const val timeoutInSec = 60L
    }
}

@InstallIn(SingletonComponent::class)
@Module
interface NetworkBind {
    @Binds
    @Singleton
    @LoggingInterceptor
    fun bindInterceptor(impl: LoggingInterceptorImpl): Interceptor
}