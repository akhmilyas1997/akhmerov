package com.akhmerov.tinkoff.di

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class DefaultServerUrl

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class LoggingInterceptor