package com.akhmerov.tinkoff.di

import com.akhmerov.tinkoff.core.DispatcherProvider
import com.akhmerov.tinkoff.core.DispatcherProviderImp
import com.akhmerov.tinkoff.core.ErrorProcessor
import com.akhmerov.tinkoff.core.ErrorProcessorImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
abstract class AppModule {

    @Binds
    @Singleton
    abstract fun bindErrorProcessor(impl: ErrorProcessorImpl): ErrorProcessor

    @Binds
    @Singleton
    abstract fun bindDispatcherProvider(impl: DispatcherProviderImp): DispatcherProvider
}