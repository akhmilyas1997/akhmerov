# Tinkoff Fintech Android

### Built With 🛠

- [Kotlin](https://kotlinlang.org/)
- [Coroutines](https://kotlinlang.org/docs/reference/coroutines-overview.html)
    - [StateFlow](https://developer.android.com/kotlin/flow/stateflow-and-sharedflow#stateflow)

- [Android Architecture Components](https://developer.android.com/topic/libraries/architecture)
    - [View Binding](https://developer.android.com/topic/libraries/view-binding)
        - [ViewBindingPropertyDelegate](https://github.com/androidbroadcast/ViewBindingPropertyDelegate)
    - [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel)
        - [Lifecycle-runtime](https://medium.com/androiddevelopers/a-safer-way-to-collect-flows-from-android-uis-23080b1f8bda)
    - [Hilt](https://developer.android.com/training/dependency-injection/hilt-android)

- Network layer
    - [Retrofit](https://github.com/square/retrofit)
    - [OkHttp](https://github.com/square/okhttp)
    - [Moshi](https://github.com/square/moshi)

- Image loading
    - [Glide](https://github.com/bumptech/glide)

### App screenshots
<img alt="Icon" src="images/dark_theme_loading.jpg" width="250" /> <img alt="Icon" src="images/dark_theme_success.jpg" width="250" /> <img alt="Icon" src="images/dark_theme_error.jpg" width="250" /> 

<img alt="Icon" src="images/white_theme_loading.jpg" width="250" /> <img alt="Icon" src="images/white_theme_success.jpg" width="250" /> <img alt="Icon" src="images/white_theme_error.jpg" width="250" /> 
